/*
    WakeOnLANLIB - Wake On LAN library for waking up your machines.
    Copyright (C) 2010  Steven Rodriguez

    This program is part of WakeOnLANLIB.

    WakeOnLANLIB is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//WakeOnLANLIB (2010 - Steven Rodriguez -)                                //
//=====================================================================//

////////////////////////////////////////////////////////////////////
/// \file wakeonlan.h
/// \brief WakeOnLANLIB header file.
/// \details This is the WakeOnLANLIB header file.
////////////////////////////////////////////////////////////////////

//===========================================================//
//Libraries                                                  //
//===========================================================//

#include <stdio.h>
#include <math.h>
#include <SDL/SDL_net.h>
#include <glib.h>

//===========================================================//
//Macros                                                     //
//===========================================================//

////////////////////////////////////////////////////
/// \def WAKEONLAN_VERSION
/// \brief The WakeOnLANLIB version.
////////////////////////////////////////////////////
#define WAKEONLAN_VERSION 1

////////////////////////////////////////////////////
/// \def WAKEONLAN_VENDOR
/// \brief The WakeOnLANLIB vendor.
////////////////////////////////////////////////////
#define WAKEONLAN_VENDOR 2

////////////////////////////////////////////////////
/// \def WAKEONLAN_COMPILE_DATE
/// \brief The WakeOnLANLIB compile date.
////////////////////////////////////////////////////
#define WAKEONLAN_COMPILE_DATE 3

////////////////////////////////////////////////////
/// \def WAKEONLAN_TRUE
/// \brief Boolean TRUE value of WakeOnLANLIB.
////////////////////////////////////////////////////
#define WAKEONLAN_TRUE 1

////////////////////////////////////////////////////
/// \def WAKEONLAN_FALSE
/// \brief Boolean FALSE value of WakeOnLANLIB.
////////////////////////////////////////////////////
#define WAKEONLAN_FALSE 0

//===========================================================//
//Types                                                      //
//===========================================================//

////////////////////////////////////////////////////
/// \typedef guint8 WakeOnLAN_UInt8
/// \brief WakeOnLANLIB 8-bit unsigned integer.
////////////////////////////////////////////////////
typedef guint8 WakeOnLAN_UInt8;

////////////////////////////////////////////////////
/// \typedef guint16 WakeOnLAN_UInt16
/// \brief WakeOnLANLIB 16-bit unsigned integer.
////////////////////////////////////////////////////
typedef guint16 WakeOnLAN_UInt16;

//===========================================================//
//Enumerations                                               //
//===========================================================//

////////////////////////////////////////////////////
/// \enum WakeOnLANError
/// \brief WakeOnLANLIB error types.
////////////////////////////////////////////////////
enum WakeOnLANError
{
	WAKEONLAN_NO_ERROR = 0,
	WAKEONLAN_INVALID_MAC_HEX_CONVERSION = 1,
	WAKEONLAN_INVALID_MAC_STRING = 2,
	WAKEONLAN_INVALID_SECUREON_STRING = 3,
	WAKEONLAN_INVALID_IP_INT_CONVERSION = 4,
	WAKEONLAN_INVALID_IP_STRING = 5,
	WAKEONLAN_CANT_INIT_SYSTEM = 6,
	WAKEONLAN_CANT_CREATE_SOCKET = 7,
	WAKEONLAN_CANT_CREATE_PACKET = 8,
	WAKEONLAN_CANT_RESOLVE_HOST = 9,
	WAKEONLAN_CANT_SEND_PACKET = 10
};

//===========================================================//
//Structures                                                 //
//===========================================================//

////////////////////////////////////////////////////
/// \struct MACAddress
/// \brief Base MAC Address structure.
////////////////////////////////////////////////////
struct MACAddress
{
	WakeOnLAN_UInt8 Address[6];
};

////////////////////////////////////////////////////
/// \struct IPAddress
/// \brief Base IP Address structure.
////////////////////////////////////////////////////
struct IPAddress
{
	WakeOnLAN_UInt8 Address[4];
};

////////////////////////////////////////////////////
/// \struct SecureOnPassword
/// \brief Base SecureOn (tm) password structure.
////////////////////////////////////////////////////
struct SecureOnPassword
{
	WakeOnLAN_UInt8 Password[6];
};

//===========================================================//
//Functions                                                  //
//===========================================================//

//******************************************************//
//General library functions                             //
//******************************************************//

////////////////////////////////////////////////////
/// \fn WakeOnLAN_UInt8 WakeOnLAN_GetError()
/// \brief Gets the last WakeOnLANLIB error.
/// \return The last WakeOnLANLIB error.
////////////////////////////////////////////////////
WakeOnLAN_UInt8 WakeOnLAN_GetError();

////////////////////////////////////////////////////
/// \fn char *WakeOnLAN_GetErrorString()
/// \brief Gets the last WakeOnLANLIB error string.
/// \return The last WakeOnLANLIB error string.
////////////////////////////////////////////////////
char *WakeOnLAN_GetErrorString();

////////////////////////////////////////////////////
/// \fn void WakeOnLAN_SetError(WakeOnLAN_UInt8 error)
/// \brief Sets the last WakeOnLANLIB error.
/// \param error The error to set.
////////////////////////////////////////////////////
void WakeOnLAN_SetError(WakeOnLAN_UInt8 error);

////////////////////////////////////////////////////
/// \fn void WakeOnLAN_ClearErrors()
/// \brief Clears the errors of the WakeOnLANLIB.
////////////////////////////////////////////////////
void WakeOnLAN_ClearErrors();

////////////////////////////////////////////////////
/// \fn const char *WakeOnLAN_GetString(WakeOnLAN_UInt8 attribute)
/// \brief Gets some WakeOnLANLIB information string.
/// \param attribute The WakeOnLANLIB attribute to get.
/// \return A string containing the selected attribute or NULL if the attribute is invalid.
////////////////////////////////////////////////////
const char *WakeOnLAN_GetString(WakeOnLAN_UInt8 attribute);

//******************************************************//
//Wake On LAN functions                                 //
//******************************************************//

////////////////////////////////////////////////////
/// \fn struct MACAddress WakeOnLAN_CreateMACAddress(WakeOnLAN_UInt8 b1, WakeOnLAN_UInt8 b2, WakeOnLAN_UInt8 b3, WakeOnLAN_UInt8 b4, WakeOnLAN_UInt8 b5, WakeOnLAN_UInt8 b6)
/// \brief Creates a MAC Address based on the 6 unsigned 8-bit integers.
/// \param b1 The first unsigned 8-bit integer of the MAC Address.
/// \param b2 The second unsigned 8-bit integer of the MAC Address.
/// \param b3 The third unsigned 8-bit integer of the MAC Address.
/// \param b4 The fourth unsigned 8-bit integer of the MAC Address.
/// \param b5 The fifth unsigned 8-bit integer of the MAC Address.
/// \param b6 The sixth unsigned 8-bit integer of the MAC Address.
/// \return A structure containing the MAC Address created.
////////////////////////////////////////////////////
struct MACAddress WakeOnLAN_CreateMACAddress(WakeOnLAN_UInt8 b1, WakeOnLAN_UInt8 b2, WakeOnLAN_UInt8 b3, WakeOnLAN_UInt8 b4, WakeOnLAN_UInt8 b5, WakeOnLAN_UInt8 b6);

////////////////////////////////////////////////////
/// \fn struct MACAddress WakeOnLAN_CreateMACAddressFromString(char *macAddress)
/// \brief Creates a MAC Address based on a string representation.
/// \details If fails sets the WAKEONLAN_INVALID_MAC_STRING error flag.
/// \param macAddress The string representation of the MAC Address.
/// \return A structure containing the MAC Address created.
////////////////////////////////////////////////////
struct MACAddress WakeOnLAN_CreateMACAddressFromString(char *macAddress);

////////////////////////////////////////////////////
/// \fn char *WakeOnLAN_CreateStringFromMACAddress(struct MACAddress macAddress)
/// \brief Creates a MAC Address string representation based on a MAC address.
/// \param macAddress The MAC address to convert.
/// \return A string representation of the MAC address (this value must be freed).
////////////////////////////////////////////////////
char *WakeOnLAN_CreateStringFromMACAddress(struct MACAddress macAddress);

////////////////////////////////////////////////////
/// \fn struct SecureOnPassword WakeOnLAN_CreateSecureOnPassword(WakeOnLAN_UInt8 b1, WakeOnLAN_UInt8 b2, WakeOnLAN_UInt8 b3, WakeOnLAN_UInt8 b4, WakeOnLAN_UInt8 b5, WakeOnLAN_UInt8 b6)
/// \brief Creates a SecureOn (tm) password based on the 6 unsigned 8-bit integers.
/// \param b1 The first unsigned 8-bit integer of the SecureOn (tm) password.
/// \param b2 The second unsigned 8-bit integer of the SecureOn (tm) password.
/// \param b3 The third unsigned 8-bit integer of theSecureOn (tm) password.
/// \param b4 The fourth unsigned 8-bit integer of the SecureOn (tm) password.
/// \param b5 The fifth unsigned 8-bit integer of the SecureOn (tm) password.
/// \param b6 The sixth unsigned 8-bit integer of the SecureOn (tm) password.
/// \return A structure containing the SecureOn (tm) password created.
////////////////////////////////////////////////////
struct SecureOnPassword WakeOnLAN_CreateSecureOnPassword(WakeOnLAN_UInt8 b1, WakeOnLAN_UInt8 b2, WakeOnLAN_UInt8 b3, WakeOnLAN_UInt8 b4, WakeOnLAN_UInt8 b5, WakeOnLAN_UInt8 b6);

////////////////////////////////////////////////////
/// \fn struct SecureOnPassword WakeOnLAN_CreateSecureOnPasswordFromString(char *secureOnPassword)
/// \brief Creates a SecureOn (tm) password based on a string representation.
/// \details If fails sets the WAKEONLAN_INVALID_SECUREON_STRING error flag.
/// \param macAddress The string representation of the SecureOn (tm) password.
/// \return A structure containing the SecureOn (tm) password created.
////////////////////////////////////////////////////
struct SecureOnPassword WakeOnLAN_CreateSecureOnPasswordFromString(char *secureOnPassword);

////////////////////////////////////////////////////
/// \fn char *WakeOnLAN_CreateStringFromSecureOnPassword(struct SecureOnPassword secureOnPassword)
/// \brief Creates a SecureOn (tm) password string representation based on a SecureOn (tm) password.
/// \param macAddress The SecureOn (tm) password to convert.
/// \return A string representation of the SecureOn (tm) password (this value must be freed).
////////////////////////////////////////////////////
char *WakeOnLAN_CreateStringFromSecureOnPassword(struct SecureOnPassword secureOnPassword);

////////////////////////////////////////////////////
/// \fn struct IPAddress WakeOnLAN_CreateIPAddress(WakeOnLAN_UInt8 b1, WakeOnLAN_UInt8 b2, WakeOnLAN_UInt8 b3, WakeOnLAN_UInt8 b4)
/// \brief Creates an IP Address based on the 4 unsigned 8-bit integers.
/// \param b1 The first unsigned 8-bit integer of the IP Address.
/// \param b2 The second unsigned 8-bit integer of the IP Address.
/// \param b3 The third unsigned 8-bit integer of the IP Address.
/// \param b4 The fourth unsigned 8-bit integer of the IP Address.
/// \return A structure containing the IP Address created.
////////////////////////////////////////////////////
struct IPAddress WakeOnLAN_CreateIPAddress(WakeOnLAN_UInt8 b1, WakeOnLAN_UInt8 b2, WakeOnLAN_UInt8 b3, WakeOnLAN_UInt8 b4);

////////////////////////////////////////////////////
/// \fn struct IPAddress WakeOnLAN_CreateIPAddressFromString(char *ipAddress)
/// \brief Creates a IP Address based on a string representation.
/// \details If fails sets the WAKEONLAN_INVALID_IP_STRING error flag.
/// \param ipAddress The string representation of the IP Address.
/// \return A structure containing the IP Address created.
////////////////////////////////////////////////////
struct IPAddress WakeOnLAN_CreateIPAddressFromString(char *ipAddress);

////////////////////////////////////////////////////
/// \fn char *WakeOnLAN_CreateStringFromIPAddress(struct IPAddress ipAddress)
/// \brief Creates a IP Address string representation based on a IP address.
/// \param macAddress The IP address to convert.
/// \return A string representation of the IP address (this value must be freed).
////////////////////////////////////////////////////
char *WakeOnLAN_CreateStringFromIPAddress(struct IPAddress ipAddress);

////////////////////////////////////////////////////
/// \fn WakeOnLAN_UInt8 WakeOnLAN_WakeupMachine(struct MACAddress macAddress, struct IPAddress ipAddress, WakeOnLAN_UInt16 port)
/// \brief Sends the magic UDP packet to wakeup the specified machine.
/// \details If fails sets the WAKEONLAN_CANT_INIT_SYSTEM, WAKEONLAN_CANT_CREATE_SOCKET, WAKEONLAN_CANT_RESOLVE_HOST, WAKEONLAN_CANT_CREATE_PACKET or WAKEONLAN_CANT_SEND_PACKET error flag.
/// \param macAddress The machine MAC Address to wakeup.
/// \param ipAddress The machine IP Address to wakeup.
/// \param port The machine UDP port to wakeup.
/// \param secureOnPassword The SecureOn (tm) password to wakeup.
/// \param useSecureOnPassword LANWAKEUP_TRUE to use the SecureOn (tm) password selected and LANWAKEUP_FALSE if not.
/// \return WAKEONLAN_TRUE if wakeup was sucessfull or WAKEONLAN_FALSE if not.
////////////////////////////////////////////////////
WakeOnLAN_UInt8 WakeOnLAN_WakeupMachine(struct MACAddress macAddress, struct IPAddress ipAddress, WakeOnLAN_UInt16 port, struct SecureOnPassword secureOnPassword, WakeOnLAN_UInt8 useSecureOnPassword);

//******************************************************//
//Internal utility functions                            //
//******************************************************//

////////////////////////////////////////////////////
/// \fn WakeOnLAN_UInt8 WakeOnLAN_MACHexStringToByte(char *hexString)
/// \brief Converts a MAC hex digit representation to an unsigned 8-bit integer.
/// \details If fails sets the WAKEONLAN_INVALID_MAC_HEX_CONVERSION error flag.
/// \param hexString The MAC hex digit representation to convert.
/// \return The 8-bit unsigned integer of the MAC hex digit representation.
////////////////////////////////////////////////////
WakeOnLAN_UInt8 WakeOnLAN_MACHexStringToByte(char *hexString);

////////////////////////////////////////////////////
/// \fn WakeOnLAN_UInt8 WakeOnLAN_IPIntStringToByte(char *ipString)
/// \brief Converts a IP digit representation to an unsigned 8-bit integer.
/// \details If fails sets the WAKEONLAN_INVALID_IP_INT_CONVERSION error flag.
/// \param ipString The IP digit representation to convert.
/// \return The 8-bit unsigned integer of the IP digit representation.
////////////////////////////////////////////////////
WakeOnLAN_UInt8 WakeOnLAN_IPIntStringToByte(char *ipString);

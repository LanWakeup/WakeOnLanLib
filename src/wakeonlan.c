/*
    WakeOnLANLIB - Wake On LAN library for waking up your machines.
    Copyright (C) 2010  Steven Rodriguez

    This program is part of WakeOnLANLIB.

    WakeOnLANLIB is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    If the library has problems please contact me at:

    stevencrc@digitecnology.zapto.org
*/

//=====================================================================//
//WakeOnLANLIB (2010 - Steven Rodriguez -)                                //
//=====================================================================//


//===========================================================//
//Libraries                                                  //
//===========================================================//

#include "wakeonlan.h"

//===========================================================//
//Global Variables                                           //
//===========================================================//

WakeOnLAN_UInt8 currenterror = WAKEONLAN_NO_ERROR;

//===========================================================//
//Functions                                                  //
//===========================================================//

//******************************************************//
//General library functions                             //
//******************************************************//

//TESTED
WakeOnLAN_UInt8 WakeOnLAN_GetError()
{
	return currenterror;
}

//TESTED
char *WakeOnLAN_GetErrorString()
{
	switch(currenterror)
	{
		case WAKEONLAN_INVALID_MAC_HEX_CONVERSION:
			return "The MAC hexadecimal token is invalid";
		case WAKEONLAN_INVALID_MAC_STRING:
			return "The MAC string is invalid";
		case WAKEONLAN_INVALID_SECUREON_STRING:
			return "The SecureOn (tm) string is invalid";
		case WAKEONLAN_INVALID_IP_INT_CONVERSION:
			return "The IP integer token is invalid";
		case WAKEONLAN_INVALID_IP_STRING:
			return "The IP string is invalid";
		case WAKEONLAN_CANT_INIT_SYSTEM:
			return "Could not initialize network system";
		case WAKEONLAN_CANT_CREATE_SOCKET:
			return "Could not create UDP socket";
		case WAKEONLAN_CANT_CREATE_PACKET:
			return "Could not create UDP packet";
		case WAKEONLAN_CANT_RESOLVE_HOST:
			return "Could not resolve host";
		case WAKEONLAN_CANT_SEND_PACKET:
			return "Could not send UDP packet";
		default:
			return "There is no error";
	}
}

//TESTED
void WakeOnLAN_SetError(WakeOnLAN_UInt8 error)
{
	currenterror = error;
}

//TESTED
void WakeOnLAN_ClearErrors()
{
	currenterror = WAKEONLAN_NO_ERROR;
}

//TESTED
const char *WakeOnLAN_GetString(WakeOnLAN_UInt8 attribute)
{
	switch(attribute)
	{
		case WAKEONLAN_VERSION:
			return "0.1";
		case WAKEONLAN_VENDOR:
			return "Steven Rodriguez (stevencrc@digitecnology.zapto.org)";
		case WAKEONLAN_COMPILE_DATE:
			return __DATE__;
	}

	return NULL;
}

//******************************************************//
//Wake On LAN functions                                 //
//******************************************************//

//TESTED
struct MACAddress WakeOnLAN_CreateMACAddress(WakeOnLAN_UInt8 b1, WakeOnLAN_UInt8 b2, WakeOnLAN_UInt8 b3, WakeOnLAN_UInt8 b4, WakeOnLAN_UInt8 b5, WakeOnLAN_UInt8 b6)
{
	struct MACAddress address;

	address.Address[0] = b1;
	address.Address[1] = b2;
	address.Address[2] = b3;
        address.Address[3] = b4;
        address.Address[4] = b5;
        address.Address[5] = b6;

	return address;
}

//TESTED
struct MACAddress WakeOnLAN_CreateMACAddressFromString(char *macAddress)
{
	struct MACAddress address;
	gchar **tokens;
	int i;

	//Check if the MAC address string is NULL
	if(macAddress == NULL)
	{
		WakeOnLAN_SetError(WAKEONLAN_INVALID_MAC_STRING);
                return address;
	}

	//Check if the MAC String is in the correct format
	tokens = g_strsplit(macAddress, ":", -1);

	if(g_strv_length(tokens) != 6)
	{
		g_strfreev(tokens);
		WakeOnLAN_SetError(WAKEONLAN_INVALID_MAC_STRING);
		return address;
	}

	//Check if the MAC String contains valid tokens
	for(i = 0; i < 6; i++)
	{
		address.Address[i] = WakeOnLAN_MACHexStringToByte(tokens[i]);

		if(WakeOnLAN_GetError() == WAKEONLAN_INVALID_MAC_HEX_CONVERSION)
		{
			g_strfreev(tokens);
                	WakeOnLAN_SetError(WAKEONLAN_INVALID_MAC_STRING);
                	return address;
		}
	}

	g_strfreev(tokens);

	return address;
}

//TESTED
char *WakeOnLAN_CreateStringFromMACAddress(struct MACAddress macAddress)
{
	GString *address = g_string_new(NULL);

	g_string_printf(address, "%.2x:%.2x:%.2x:%.2x:%.2x:%.2x", macAddress.Address[0], macAddress.Address[1], macAddress.Address[2], macAddress.Address[3], macAddress.Address[4], macAddress.Address[5]);

	return g_string_free(address, FALSE);
}

//TESTED
struct SecureOnPassword WakeOnLAN_CreateSecureOnPassword(WakeOnLAN_UInt8 b1, WakeOnLAN_UInt8 b2, WakeOnLAN_UInt8 b3, WakeOnLAN_UInt8 b4, WakeOnLAN_UInt8 b5, WakeOnLAN_UInt8 b6)
{
	struct SecureOnPassword password;

	password.Password[0] = b1;
	password.Password[1] = b2;
	password.Password[2] = b3;
        password.Password[3] = b4;
        password.Password[4] = b5;
        password.Password[5] = b6;

	return password;
}

//TESTED
struct SecureOnPassword WakeOnLAN_CreateSecureOnPasswordFromString(char *secureOnPassword)
{
	struct SecureOnPassword password;
	gchar **tokens;
	int i;

	//Check if the SecureOn (tm) password string is NULL
	if(secureOnPassword == NULL)
	{
		WakeOnLAN_SetError(WAKEONLAN_INVALID_SECUREON_STRING);
                return password;
	}

	//Check if the SecureOn (tm) password is in the correct format
	tokens = g_strsplit(secureOnPassword, ":", -1);

	if(g_strv_length(tokens) != 6)
	{
		g_strfreev(tokens);
		WakeOnLAN_SetError(WAKEONLAN_INVALID_SECUREON_STRING);
		return password;
	}

	//Check if the SecureOn (tm) password contains valid tokens
	for(i = 0; i < 6; i++)
	{
		password.Password[i] = WakeOnLAN_MACHexStringToByte(tokens[i]);

		if(WakeOnLAN_GetError() == WAKEONLAN_INVALID_MAC_HEX_CONVERSION)
		{
			g_strfreev(tokens);
                	WakeOnLAN_SetError(WAKEONLAN_INVALID_SECUREON_STRING);
                	return password;
		}
	}

	g_strfreev(tokens);

	return password;
}

//TESTED
char *WakeOnLAN_CreateStringFromSecureOnPassword(struct SecureOnPassword secureOnPassword)
{
	GString *password = g_string_new(NULL);

	g_string_printf(password, "%.2x:%.2x:%.2x:%.2x:%.2x:%.2x", secureOnPassword.Password[0], secureOnPassword.Password[1], secureOnPassword.Password[2], secureOnPassword.Password[3], secureOnPassword.Password[4], secureOnPassword.Password[5]);

	return g_string_free(password, FALSE);
}

//TESTED
struct IPAddress WakeOnLAN_CreateIPAddress(WakeOnLAN_UInt8 b1, WakeOnLAN_UInt8 b2, WakeOnLAN_UInt8 b3, WakeOnLAN_UInt8 b4)
{
	struct IPAddress address;

        address.Address[0] = b1;
        address.Address[1] = b2;
        address.Address[2] = b3;
        address.Address[3] = b4;

	return address;
}

//TESTED
struct IPAddress WakeOnLAN_CreateIPAddressFromString(char *ipAddress)
{
	struct IPAddress address;
        gchar **tokens;
        int i;

	//Check if the IP address string is NULL
        if(ipAddress == NULL)
        {
                WakeOnLAN_SetError(WAKEONLAN_INVALID_IP_STRING);
                return address;
        }

	//Check if the IP String is in the correct format
	tokens = g_strsplit(ipAddress, ".", -1);

        if(g_strv_length(tokens) != 4)
        {
                g_strfreev(tokens);
                WakeOnLAN_SetError(WAKEONLAN_INVALID_IP_STRING);
                return address;
        }

	//Check if the IP String contains valid tokens
        for(i = 0; i < 4; i++)
        {
                address.Address[i] = WakeOnLAN_IPIntStringToByte(tokens[i]);

		if(WakeOnLAN_GetError() == WAKEONLAN_INVALID_IP_INT_CONVERSION)
                {
                        g_strfreev(tokens);
                        WakeOnLAN_SetError(WAKEONLAN_INVALID_IP_STRING);
                        return address;
                }
        }

        g_strfreev(tokens);

	return address;
}

//TESTED
char *WakeOnLAN_CreateStringFromIPAddress(struct IPAddress ipAddress)
{
        GString *address = g_string_new(NULL);

        g_string_printf(address, "%hu.%hu.%hu.%hu", ipAddress.Address[0], ipAddress.Address[1], ipAddress.Address[2], ipAddress.Address[3]);

        return g_string_free(address, FALSE);
}

//TESTED
WakeOnLAN_UInt8 WakeOnLAN_WakeupMachine(struct MACAddress macAddress, struct IPAddress ipAddress, WakeOnLAN_UInt16 port, struct SecureOnPassword secureOnPassword, WakeOnLAN_UInt8 useSecureOnPassword)
{
	UDPsocket socket;
	UDPpacket *packet;
	IPaddress address;
	GString *string;
	int i, a;

	//Initialize SDL_Net
	if(SDLNet_Init() == -1)
	{
		WakeOnLAN_SetError(WAKEONLAN_CANT_INIT_SYSTEM);
		return WAKEONLAN_FALSE;
	}

	//Create the UDP socket
	socket = SDLNet_UDP_Open(0);

	if(socket == NULL)
	{
		WakeOnLAN_SetError(WAKEONLAN_CANT_CREATE_SOCKET);
		SDLNet_Quit();
                return WAKEONLAN_FALSE;
	}

	//Resolve the IP address and the port
	string  = g_string_new(NULL);

	g_string_printf(string, "%hu.%hu.%hu.%hu", ipAddress.Address[0], ipAddress.Address[1], ipAddress.Address[2], ipAddress.Address[3]);

	if(SDLNet_ResolveHost(&address, string->str, port) == -1)
	{
		WakeOnLAN_SetError(WAKEONLAN_CANT_RESOLVE_HOST);
		g_string_free(string, TRUE);
		SDLNet_UDP_Close(socket);
		SDLNet_Quit();
                return WAKEONLAN_FALSE;
	}

	//Create Wake On LAN packet data
	string = g_string_erase(string, 0, -1);

	for(i = 0; i < 6; i++)
	{
		string = g_string_append_c(string, 0xFF);
	}

	for(i = 0; i < 16; i++)
	{
		for(a = 0; a < 6; a++)
		{
			string = g_string_append_c(string, macAddress.Address[a]);
		}
	}

	if(useSecureOnPassword == WAKEONLAN_TRUE)
	{
		for(i = 0; i < 6; i++)
		{
			string = g_string_append_c(string, secureOnPassword.Password[i]);
		}
	}

	//Create UDP packet
	packet = SDLNet_AllocPacket(string->len);

        if(packet == NULL)
        {
                WakeOnLAN_SetError(WAKEONLAN_CANT_CREATE_PACKET);
		g_string_free(string, TRUE);
		SDLNet_UDP_Close(socket);
		SDLNet_Quit();
                return WAKEONLAN_FALSE;
        }

	packet->len = string->len;
	packet->data = g_string_free(string, FALSE);
	packet->address = address;

	//Send Wake On LAN packet
	if(SDLNet_UDP_Send(socket, -1, packet) == 0)
	{
		WakeOnLAN_SetError(WAKEONLAN_CANT_SEND_PACKET);
		SDLNet_FreePacket(packet);
		SDLNet_UDP_Close(socket);
                SDLNet_Quit();
                return WAKEONLAN_FALSE;
	}

	//Free the resources
	SDLNet_FreePacket(packet);
	SDLNet_UDP_Close(socket);
	SDLNet_Quit();

	return WAKEONLAN_TRUE;
}

//******************************************************//
//Internal utility functions                            //
//******************************************************//

//TESTED
WakeOnLAN_UInt8 WakeOnLAN_MACHexStringToByte(char *hexString)
{
	WakeOnLAN_UInt8 result = 0;
	int i, a;

	//Check if the MAC address hex string is NULL
        if(hexString == NULL)
        {
                WakeOnLAN_SetError(WAKEONLAN_INVALID_MAC_HEX_CONVERSION);
                return result;
        }

	//Check the length of the MAC String
	if(strlen(hexString) != 2)
	{
		WakeOnLAN_SetError(WAKEONLAN_INVALID_MAC_HEX_CONVERSION);
		return result;
	}

	//Check if the MAC String has only hex digits
	if(g_ascii_isxdigit(hexString[0]) == FALSE || g_ascii_isxdigit(hexString[1]) == FALSE)
	{
		WakeOnLAN_SetError(WAKEONLAN_INVALID_MAC_HEX_CONVERSION);
                return result;
	}

	//Convert the MAC hex value to decimal
	for(i = 0, a = 1; i < 2; i++, a--)
	{
		if(hexString[i] >= '0' && hexString[i] <= '9')
		{
			result += (hexString[i] - '0') * (WakeOnLAN_UInt8)pow(16, a);
		}
		else
		{
			result += (g_ascii_toupper(hexString[i]) - 'A' + 10) * (WakeOnLAN_UInt8)pow(16, a);
		}
	}

	return result;
}

//TESTED
WakeOnLAN_UInt8 WakeOnLAN_IPIntStringToByte(char *intString)
{
	WakeOnLAN_UInt8 result = 0;
	gint64 conversion;
	int i;

	//Check if the IP address Int String is NULL
        if(intString == NULL)
        {
                WakeOnLAN_SetError(WAKEONLAN_INVALID_IP_INT_CONVERSION);
                return result;
        }

	//Check the length of the IP Int String
	if(strlen(intString) <= 0)
	{
		WakeOnLAN_SetError(WAKEONLAN_INVALID_IP_INT_CONVERSION);
		return result;
	}

	//Check if the IP Int String has only digits
	for(i = 0; i < strlen(intString); i++)
	{
		if(g_ascii_isdigit(intString[i]) == FALSE)
		{
			WakeOnLAN_SetError(WAKEONLAN_INVALID_IP_INT_CONVERSION);
                	return result;
		}
	}

	//Convert the IP Int String to an Integer
	conversion = g_ascii_strtoll(intString, NULL, 0);

	//Check if the IP Int String has the correct bounds
	if(conversion < 0 | conversion > 255)
	{
		WakeOnLAN_SetError(WAKEONLAN_INVALID_IP_INT_CONVERSION);
                return result;
	}

	result = (WakeOnLAN_UInt8)conversion;

	return result;
}

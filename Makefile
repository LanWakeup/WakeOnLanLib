################################
#Makefile for WakeOnLANLIB
################################

CC = gcc
CC_FLAGS = -fPIC
SRC_DIR = src/
LIB_DIR = lib/
PKG_CONFIG_DIR = /usr/lib/pkgconfig/
SDL_CONFIG = `sdl-config --cflags --libs`
GLIB_CONFIG = `pkg-config --cflags --libs glib-2.0`
SYSTEM_HEADERS_DIR = /usr/include/
SYSTEM_LIBRARIES_DIR = /usr/lib/

all:
	#Compile library
	echo "Compiling WakeOnLANLIB..."
	$(CC) $(SDL_CONFIG) -c $(CC_FLAGS) -o $(LIB_DIR)wakeonlan.o -I $(SRC_DIR) $(GLIB_CONFIG) $(SRC_DIR)wakeonlan.c
	$(CC) -shared -lSDL_net $(CC_FLAGS) -o $(LIB_DIR)libwakeonlan.so $(LIB_DIR)wakeonlan.o

	#Remove .o thrash
	echo "Removing thrash..."
	rm $(LIB_DIR)wakeonlan.o
docs:
	#Create documentation
	echo "Creating documentation"
	doxygen doc/WakeOnLANLIB
docs_clean:
	#Clean documentation
	echo "Cleaning documentation..."
	rm -rf doc/html
install:
	#Install headers
	echo "Making directory for WakeOnLANLIB system headers if not exists..."
	mkdir -p $(SYSTEM_HEADERS_DIR)wakeonlan/
	cp $(SRC_DIR)wakeonlan.h $(SYSTEM_HEADERS_DIR)wakeonlan/

	#Install library
	echo "Installing libwakeonlan..."
	cp $(LIB_DIR)libwakeonlan.so $(SYSTEM_LIBRARIES_DIR)libwakeonlan.so

	#Install pkgconfig pc file
	echo "Installing pkg-config pc file..."
	cp pkgconfig/wakeonlan.pc $(PKG_CONFIG_DIR)wakeonlan.pc
uninstall:
	#Remove the components
	echo "Removing WakeOnLANLIB..."
	rm $(SYSTEM_LIBRARIES_DIR)libwakeonlan.so
	echo "Removing WakeOnLANLIB header..."
	rm $(SYSTEM_HEADERS_DIR)wakeonlan/wakeonlan.h
	echo "Removing directories created in installation..."
	rmdir $(SYSTEM_HEADERS_DIR)wakeonlan/
	echo "Removing pkg-config pc file..."
	rm $(PKG_CONFIG_DIR)wakeonlan.pc
clean:
	#Cleans compiling thrash
	echo "Cleaning thrash..."
	rm -f $(LIB_DIR)wakeonlan.o
	rm -f $(LIB_DIR)libwakeonlan.so
